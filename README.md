    Update: FIDELIO started its production test-phase on 2018-10-19.

During this phase FIDELIO was tested within a friendly-user and expert community
with real "production" German eID cards.

    To use FIDELIO and your official German eID as FIDO U2F and WebAuthn security token follow these steps.

- ensure you activated the "online function", i.e. you got a PIN letter and activated the eID function by setting your own eID PIN

Desktop / Laptop environments:

- get a RFID reader device suitable to work with the German eID (i.e. https://www.ausweisapp.bund.de/kompatible-geraete/kartenlesegeraete/)
- install your favourite eID client (i.e. AusweisApp2 -> https://www.ausweisapp.bund.de/ausweisapp2-home/)
- either use Chrome as the recommended browser or Firefox, Edge, Opera, Safari as "known to work"
- install the Tampermonkey extension in your browser (for Chrome: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
- install the FIDELIO user script by just clicking https://id1.vx4.eu/fidelio/js/fidelio.user.js

Mobile environment

- get an Android phone with NFC known to work with the German eID (i.e. https://www.ausweisapp.bund.de/mobile-geraete/)
- install FIDELIO App https://play.google.com/store/apps/details?id=de.persoapp.android.FIDELIO (currently by joining the beta program) from Google Play


- use FIDELIO with Facebook, GitHub, GitLab and many other U2F supporting web-sites

